//
//  myArray.hpp
//  2D
//
//  Created by Hiroshi Sugimoto on 2021/05/31.
//

#ifndef myArray_hpp
#define myArray_hpp

#include <iostream>
#include <vector>

class myDoubleArray
{
	std::vector<int> _sizes;
	std::vector<double> _data;
public:
	double* data()
	{
		return _data.data();
	}
	const int size()
	{
		int sz=1;
		for(auto k:_sizes) sz*=k;
		return sz;
	}
	const int size(const int index)
	{
		return _sizes.at(index);
	}
	double& operator[](const int k)
	{
		return _data[k];
	}
	const int index(const int dim,const int k)
	{
		int _index=k;
		for(int i=0;i<dim;i++) _index/=_sizes[i];
		if (dim==_sizes.size()-1) return _index;
		return _index%_sizes[dim];
	}
	myDoubleArray(const int n0,const int n1)
	{
		_sizes.resize(2);	//2次元配列である
		_sizes[0]=n0;	_sizes[1]=n1;
		_data.resize(size());
	}
	double& operator()(const int i0,const int i1)
	{
		return _data[i0+_sizes[0]*i1];
	}
	myDoubleArray(const int n0,const int n1,const int n2)
	{
		_sizes.resize(3);	//3次元配列である
		_sizes[0]=n0;	_sizes[1]=n1;	_sizes[2]=n2;
		_data.resize(size());
	}
	double& operator()(const  int i0,const int i1,const int i2)
	{
		return _data[i0+_sizes[0]*i1+_sizes[0]*_sizes[1]*i2];
	}
};

template <class T> class myArray
{
	std::vector<int> _sizes;
	std::vector<int> _upto;
	std::vector<T> _data;
public:
	T* data()
	{
		return _data.data();
	}
	const int size()
	{
		return _upto.back();
	}
	const int size(const int index)
	{
		return _sizes.at(index);
	}
	T& operator[](const int k)
	{
		return _data[k];
	}
	typename std::vector<T>::iterator begin()
	{
		return _data.begin();
	}
	typename std::vector<T>::iterator end()
	{
		return _data.end();
	}

	const int index(const int dim,const int k)
	{
		int _index=k;
		for(int i=0;i<dim;i++) _index/=_sizes[i];
		if (dim==_sizes.size()-1) return _index;
		return _index%_sizes[dim];
	}
	myArray(const int n0,const int n1)
	{
		_sizes.resize(2);	//2次元配列である
		_upto.resize(2);
		_sizes[0]=n0;	_sizes[1]=n1;
		_upto[0]=n0;	_upto[1]=_upto[0]*n1;
		_data.resize(size());
	}
	T& operator()(const int i0,const int i1)
	{
		return _data[i0+_upto[0]*i1];
	}
	myArray(const int n0,const int n1,const int n2)
	{
		_sizes.resize(3);	//3次元配列である
		_upto.resize(3);
		_sizes[0]=n0;	_sizes[1]=n1;			_sizes[2]=n2;
		_upto[0]=n0;	_upto[1]=_upto[0]*n1;	_upto[2]=_upto[1]*n2;
		_data.resize(size());
	}
	T& operator()(const  int i0,const int i1,const int i2)
	{
		return _data[i0+_sizes[0]*i1+_sizes[0]*_sizes[1]*i2];
	}
};

#endif /* myArray_hpp */
