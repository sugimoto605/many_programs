//
//  jacobi.hpp
//  2D
//
//  Created by Hiroshi Sugimoto on 2021/06/01.
//

#ifndef jacobi_hpp
#define jacobi_hpp
#include <sstream>
#include <cmath>
#include "myArray.hpp"
#include <boost/filesystem.hpp>

class jacobi
{
	myArray<double> U,U_old;
	std::ofstream ofs;
public:
	jacobi(int N,std::string filename):U(N,N),U_old(N,N)
	{
		for(auto& uv:U) uv=0.0;
		//境界条件: x=0でU=1, 残りは0ってことで
		for(int i=0;i<U.size(0);i++) U(i,0)=1.0;
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/"tes_test");
		ofs.open(filename);
	}
	double step()
	{
		double diff=0.;
		U_old=U;
		for(int i=1;i<U.size(0)-1;i++) for(int j=1;j<U.size(1)-1;j++)
		{
			U(i,j)=(U_old(i-1,j)+U_old(i+1,j)+U_old(i,j-1)+U_old(i,j+1))/4.;
			diff=std::max(diff,std::abs(U(i,j)-U_old(i,j)));
		}
		return diff;
	}
	void write()
	{
		for(int i=0;i<U.size(0);i++) for(int j=0;j<U.size(1);j++)
			ofs
			<< static_cast<double>(i)/(U.size(0)-1) << " "
			<< static_cast<double>(j)/(U.size(1)-1) << " "
			<< U(i,j) <<std::endl;
	}
	void vtk(int nstep)
	{
		std::ostringstream oss;
		oss << "vtk_" <<  std::setfill('0') << std::right << std::setw(6) << nstep << ".vtk";
		std::ofstream vout(oss.str());
		vout << "# vtk DataFile Version 2.0" << std::endl << "ひゃっほう" << std::endl << "ASCII" << std::endl;
		//Geometry指定
		vout << "DATASET STRUCTURED_POINTS" << std::endl;
		vout << "DIMENSIONS " << U.size(0) << " " << U.size(1) << " " << 1 << std::endl;
		vout << "ORIGIN 0 0 0" << std::endl;
		vout << "SPACING " << 1.0/(U.size(0)-1) << " " << 1.0/(U.size(1)-1) << " " << 1.0 << std::endl;
		//DataSet指定
		vout << "POINT_DATA " << U.size(0)*U.size(1) << std::endl;
		vout << "SCALARS U double" << std::endl;
		vout << "LOOKUP_TABLE default" << std::endl;
		for(int j=0;j<U.size(1);j++) for(int i=0;i<U.size(0);i++) vout << U(i,j) <<std::endl;
	}
};
#endif /* jacobi_hpp */
