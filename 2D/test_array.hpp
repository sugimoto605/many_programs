//
//  test_array.hpp
//  2D
//
//  Created by Hiroshi Sugimoto on 2021/06/01.
//

#ifndef test_array_hpp
#define test_array_hpp

#include <boost/multi_array.hpp>
void test_boost(){
	const int ndim=3;
	using D3D=boost::multi_array<double, ndim>; // 倍精度「3次元」配列を定義
	D3D MDA(boost::extents[3][4][2]);
	boost::array<D3D::index,ndim> MDA_base = {{0, 1, -5}};
	MDA.reindex(MDA_base);
	std::cout << "dimension= " << MDA.num_dimensions() << std::endl;
	for(int k=0;k<MDA.num_dimensions();k++)
	{
		std::cout << "dim-" << k
		<< " min:size= " << MDA.index_bases()[k] << ":" << MDA.shape()[k] << std::endl;
	}
	std::cout << "total elements= " << MDA.num_elements() << std::endl;
	for(int i=0;i<MDA.shape()[0];i++)
		for(int j=0;j<MDA.shape()[1];j++)
			for(int k=0;k<MDA.shape()[2];k++)
			{
				auto er=MDA.index_bases();
				int ir=er[0]+i,jr=er[1]+j,kr=er[2]+k;
				auto val=MDA[ir][jr][kr]=(i+j*j+100*(k+1))*0.1;
				std::cout << "MDA[" << ir << "][" << jr << "][" << kr << "]= " << val << std::endl;
			}
	for(int i=0;i<MDA.num_elements();i++)
		std::cout << "MDA[" << i << "]= " << MDA.data()[i] << std::endl;
}
void test_myArray()
{
	std::cout << "テスト2D配列3x4" << std::endl;
	myArray<double> MDA(3,4);
	for(int i=0;i<MDA.size(0);i++)
		for(int j=0;j<MDA.size(1);j++)
			MDA(i,j)=(i+j*j)*0.1;
	for(int k=0;k<MDA.size();k++)
		std::cout << "Value[" << std::setw(3) << k << "]=Value("<<MDA.index(0,k)<<","<<MDA.index(1,k) << ")=" << MDA[k] << std::endl;
	std::cout<<" At(0,2) Value " << MDA(0,2) << std::endl;
	std::cout<<" At(2,3) Value " << MDA(2,3) << std::endl;
	std::cout << "テスト2D配列3x4x2" << std::endl;
	myArray<int> MDB(3,4,2);
	for(int i=0;i<MDB.size(0);i++)
		for(int j=0;j<MDB.size(1);j++)
			for(int k=0;k<MDB.size(2);k++)
				MDB(i,j,k)=(i+j*j+100*(k+1))*0.1;
	for(int k=0;k<MDB.size();k++)
		std::cout << "Value[" << std::setw(3) << k
		<< "]=Value("<<MDB.index(0,k)<<","<<MDB.index(1,k) << ","
		<< MDB.index(2,k) << ")=" << MDB[k] << std::endl;
	std::cout<<" At(0,2,0) Value " << MDB(0,2,0) << std::endl;
	std::cout<<" At(2,3,1) Value " << MDB(2,3,1) << std::endl;
}

#endif /* test_array_hpp */
