//
//  main.cpp
//  2D
//
//  Created by Hiroshi Sugimoto on 2021/05/31.
//

#include "myArray.hpp"
#include "jacobi.hpp"
#include <iomanip>
int main(int argc, const char * argv[]) {
	jacobi Solver(101,"jacobi_100.dat");
	double e=1.0;
	int n=0;
	while ((e=Solver.step())>1e-10)
	{
		if (++n%100==0)
		{
			std::cout << "Step:Diff= " << n << " " << e << std::endl;
			Solver.vtk(n);
		}
	}
	std::cout << "Finish Step:Diff= " << n << " " << e << std::endl;
	Solver.write();
	return 0;
}
