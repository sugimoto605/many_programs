//
//  流体力学1に載せる資料が作りたいにゃ
//
//  stream_tube

#include <iostream>
#include "make_vtk.hpp"
void draw_StreamPastFairing()
{//沈黙の艦隊を描く
	StreamPastFairing SPF("owncloud/FD/fd1/fig/chap3",-6.,+12.,0.01);
	SPF.dth=1;
	{
		std::ofstream vout("fairing.vtk");
		vout << SPF.draw_Fairing();
	}
	{
		std::ofstream vout("fairing_01.vtk");
		double XC=-6,YC=0.08,ZC=0.0,RAD=0.01;
		vout << SPF.full(XC,YC,ZC,RAD);
	}
	{
		std::ofstream vout("fairing_0a.vtk");
		double XC=-6,YC=0.02,ZC=0.0,RAD=0.01;
		vout << SPF.full(XC,YC,ZC,RAD);
	}
	{
		std::ofstream vout("fairing_05.vtk");
		double XC=-6,YC=0.3,ZC=0.0,RAD=0.01;
		vout << SPF.full(XC,YC,ZC,RAD);
	}
}
void draw_StreamPastCylinder()
{//流線焼き鳥を描く
	StreamPastCylinder FS("owncloud/FD/fd1/fig/chap3",-6.,+6.,0.002);
	double XC,YC,ZC,RAD;
	{
		std::ofstream vout("full.vtk");
		XC=-6;	YC=1.1;	ZC=0.0;	RAD=1;
		vout << FS.full(XC,YC,ZC,RAD);
	}
	{
		std::ofstream vout("upside.vtk");
		XC=-6;	ZC=7.0;	RAD=1;
		vout << FS.upside(XC,ZC,RAD);
	}
	{
		std::ofstream vout("loside.vtk");
		XC=-6;	ZC=7.0;	RAD=1;
		vout << FS.loside(XC,ZC,RAD);
	}
}
void draw_StreamPastArcwing()
{
	//円弧翼シリーズ
	//	double alpha=M_PI/6,beta=M_PI/24;	//リアルだがちょいとわからんな
	//	double alpha=-M_PI/6,beta=M_PI/12;	//まあ普通だな
	//	double alpha=0,beta=M_PI/12;		//芸格0
	//	double alpha=M_PI/6,beta=M_PI/12;	//まあ普通だな
	//	double alpha=M_PI/3,beta=M_PI/12;	//まあ普通だな
	//	double alpha=-M_PI/6,beta=M_PI/4;	//半円
	//	double alpha=0,beta=M_PI/4;			//半円
	//	double alpha=M_PI/3,beta=M_PI/4;	//半円
	//Joukowskiシリーズ
	//	eps=0.1でJoukowski, eps=0で円弧翼
	//	β=π/24 ちょいと太め
	//double alpha=-M_PI/36,beta=M_PI/24;	//迎角-5度
	//double alpha=0,beta=M_PI/24;			//迎角0度
	//double alpha=M_PI/18,beta=M_PI/24;	//迎角10度
	//double alpha=M_PI/18,beta=M_PI/24;	//迎角10度
	//double alpha=M_PI/36,beta=M_PI/24;	//迎角5度
	//double alpha=M_PI/6,beta=M_PI/24;		//迎角30度. これ失速するやつ
	//	β=π/96 NACA翼にかなり類似
	//double alpha=0,beta=M_PI/96,eps=0.1;	//迎角0度
	//double alpha=M_PI/96,beta=M_PI/96,eps=0.1;	//迎角0度
	//double alpha=M_PI/96,beta=M_PI/96,eps=0.1;	//迎角2度
	double alpha=M_PI/18,beta=M_PI/96,eps=0.1;	//迎角10度
	//	β=0 対称Joukowski翼
	//double alpha=-M_PI/18,beta=0;		//対称翼, 迎角-10度
	//double alpha=0,beta=0;			//対称翼, 迎角0度
	//double alpha=M_PI/36,beta=0;		//対称翼, 迎角5度
	//double alpha=M_PI/6,beta=0;		//対称翼, 迎角30度
	//ファイル名
	std::string ident="J_18_96";
	//円弧翼
	//StreamPastArcwing AW("owncloud/FD/fd1/fig/chap6/VTK",120,121,alpha,beta);
	//Joukowski翼
	StreamPastArcwing AW("owncloud/FD/fd1/fig/chap6/VTK",120,121,alpha,beta,eps);
	AW.make_zeta(0.002);
	AW.make_F();
	AW.rotate_F();
	AW.joukowski_F();
	AW.rotate_Z(-alpha);
	AW.get_P();
	if (false) {//下面の流れ関数
		std::ofstream vout("loside"+ident+".vtk");
		vout << AW.loside();
	}
	if (false) {//上面の流れ関数
		std::ofstream vout("upside"+ident+".vtk");
		vout << AW.upside();
	}
	if (true) {//翼形状
		std::ofstream vout("arcwing"+ident+".vtk");
//		vout << AW.wing(41);
		vout << AW.wing(9);
	}
	if (false) {//翼断面
		std::ofstream vout("cap"+ident+".vtk");
		vout << AW.wing(1);
	}
	if (false) {//流管1
		std::ofstream vout("up_"+ident+".vtk");
		double xc=-20;
		double rad=0.1;
		int NTH=91,NST=4000;
		double dst=0.01;
		//double yc=-1.5;	//alpha=M_PI/18,beta=M_PI/24
		double yc=-1.075;	//alpha=M_PI/18,beta=M_PI/96
		vout << AW.stream_tube(NTH,NST,dst,xc,yc,rad);
	}
	if (false) {//流管2
		std::ofstream vout("lo_"+ident+".vtk");
		double xc=-20;
		int NTH=91,NST=4000;
		double dst=0.01;
		double rad=0.1;
		//double yc=-1.9;	//alpha=M_PI/18,beta=M_PI/24
		double yc=-1.375;	//alpha=M_PI/18,beta=M_PI/96
		vout << AW.stream_tube(NTH,NST,dst,xc,yc,rad);
	}
	{//流管1MOD
		std::ofstream vout("up_thick_"+ident+".vtk");
		double xc=-5;
		double rad=0.15;
		int NTH=91,NST=2500;
		double dst=0.01;
		//double yc=-1.5;	//alpha=M_PI/18,beta=M_PI/24
		double yc=-0.380;	//alpha=M_PI/18,beta=M_PI/96
		vout << AW.stream_tube(NTH,NST,dst,xc,yc,rad);
	}
	{//流管2MOD
		std::ofstream vout("lo_thick_"+ident+".vtk");
		double xc=-5;
		double rad=0.15;
		int NTH=91,NST=2500;
		double dst=0.01;
		//double yc=-1.9;	//alpha=M_PI/18,beta=M_PI/24
		double yc=-0.715;	//alpha=M_PI/18,beta=M_PI/96
		vout << AW.stream_tube(NTH,NST,dst,xc,yc,rad);
	}
}
int main(int argc, const char * argv[]) {
	draw_StreamPastArcwing();
	return 0;
}
