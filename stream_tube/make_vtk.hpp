//
//  make_vtk.hpp
//  stream_tubeを描くParaView用VTKファイルを作成
//	http://fd.kuaero.kyoto-u.ac.jp/ja/node/357
//	BOOSTいるっす
//	http://fd.kuaero.kyoto-u.ac.jp/ja/node/354
//  Created by Hiroshi Sugimoto on 2021/09/19.
//

#ifndef make_vtk_hpp
#define make_vtk_hpp
#include <sstream>
#include <cmath>
#include "myArray.hpp"
#include "intde.hpp"
#include <boost/filesystem.hpp>
#include <boost/numeric/odeint.hpp>
#include <complex>
class StreamPastArcwing
{//ArcWingといいつつJoukowski翼にも対応しちゃったりするクラス
	double alpha;//迎角
	double beta;//翼高さ
	double eps;//Joukowskiパラメータ
	double gamma;//Kutta条件に基づく時計回り循環
	std::complex<double> zeta_0;//円弧翼を作るzeta円の中心
	double zeta_R;//円弧翼を作るzeta円の半径
	int NW;//翼面の分点数
	class Tube
	{
		class TubePoint
		{
		public:
			std::complex<double> Z;
			double depth;
			double time;
			double theta;
		};
		using ode_State=std::complex<double>;
		class ode_System
		{
			double zeta_R;
			double gamma;
		public:
			ode_System(double zeta_R,double gamma):zeta_R(zeta_R),gamma(gamma){};
			void operator()(const ode_State& x, ode_State& dx, double t)
			{
				double zX=zeta_R/std::norm(x);
				dx.real(1.-zX*zX*(x.real()*x.real()-x.imag()*x.imag())+2*gamma*x.imag()/std::norm(x));
				dx.imag(-zX*zX*2.0*x.real()*x.imag()-2.0*gamma*x.real()/std::norm(x));
			}
		};
		ode_System ode_Data;
		myArray<TubePoint> DATA;
	public:
		Tube(int NTH,int NS,double zeta_R,double gamma):DATA(NTH,NS),ode_Data(zeta_R,gamma)
		{};
		void set_C(double xc,double yc,double rad,double dx=0.1)
		{
			double d_theta=2*M_PI/(DATA.size(0)-1);
			for(int i=0;i<DATA.size(0);i++)
			{
				double theta=d_theta*i;
				DATA(i,0).Z.real(xc);
				DATA(i,0).Z.imag(yc+rad*cos(theta));
				DATA(i,0).depth=rad*sin(theta);
				DATA(i,0).theta=theta*180.0/M_PI;
				DATA(i,0).time=0.0;
			}
			//デバッグのため, とりあえず値入れてみる
			for(int i=0;i<DATA.size(0);i++)
			for(int j=1;j<DATA.size(1);j++)
			{
				DATA(i,j).Z=DATA(i,j-1).Z;
				DATA(i,j).time=DATA(i,j-1).time;
				DATA(i,j).theta=DATA(i,j-1).theta;
				DATA(i,j).depth=DATA(i,j-1).depth;
				DATA(i,j).Z.real(DATA(i,j).Z.real()+dx);
			}
		};
		void solve_stream()
		{
			//ええい面倒だ, dX/dt=u, dY/dt=v を解いてしまえ
			boost::numeric::odeint::runge_kutta4<ode_State> ode_Stepper;
			//DATA(i,0).Zを初期値にして, DATA(i,j).Z.real()におけるDATA(i,j).Z.imag()を求める
			for(int i=0;i<DATA.size(0);i++)
			{//i番目の流線を解く
				for(int j=1;j<DATA.size(1);j++)
				{
					std::complex<double> Solution=DATA(i,j-1).Z;
					double time_from=0.;
					double time_upto=0.01;
					double time_dt=0.0001;
					boost::numeric::odeint::integrate_const(ode_Stepper,ode_Data,Solution,time_from,time_upto,time_dt);
					DATA(i,j).Z=Solution;
					DATA(i,j).time=DATA(i,j-1).time+time_upto;
				}
			}
		}
		void rotate(double alpha,std::complex<double> center=0.)
		{
			std::complex<double> c;
			c.real(cos(alpha));
			c.imag(-sin(alpha));
			for(auto& D:DATA) D.Z=center+c*(D.Z-center);
		};
		void joukowski()
		{
			for(auto& D:DATA)
				D.Z=D.Z+1./D.Z;
		}
		void shift(std::complex<double> shift)
		{
			for(auto& D:DATA)
				D.Z=D.Z+shift;
		}
		void i_joukowski()
		{
			std::complex<double> c;
			c.real(0.0);
			c.imag(1.0);
			for(auto& D:DATA)
			{
				std::complex<double> z=D.Z;
				double az=std::norm(z)-4.;		//	az=|z|^2=x^2+y^2
				double s=(az+sqrt(az*az+16*z.imag()*z.imag()))/2;
				double r=(sqrt(s)+sqrt(s+4.))/2.;
				double th=atan2(z.imag()/sqrt(s),z.real()/sqrt(s+4.));
				D.Z=std::polar(r,th);
			}
		};
		std::string output()
		{
			std::ostringstream oss;
			oss << "# vtk DataFile Version 2.0\nえんこよくのりゅうせんだ\nASCII" << std::endl;
			oss << "DATASET STRUCTURED_GRID" << std::endl;
			oss << "DIMENSIONS " << DATA.size(0) << " " << DATA.size(1) << " 1" << std::endl;
			oss << "POINTS " << DATA.size() << " float" << std::endl;
			for(int j=0;j<DATA.size(1);j++)
			for(int i=0;i<DATA.size(0);i++)
				oss << DATA(i,j).Z.real() << " "
					<< DATA(i,j).Z.imag() << " "
					<< DATA(i,j).depth << std::endl;
			oss << "POINT_DATA " << DATA.size() << std::endl;
			{
				oss << "SCALARS theta double" << std::endl;
				oss << "LOOKUP_TABLE default" << std::endl;
				for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
					oss << DATA(i,j).theta << std::endl;
			}
			{
				oss << "SCALARS time double" << std::endl;
				oss << "LOOKUP_TABLE default" << std::endl;
				for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
					oss << DATA(i,j).time << std::endl;
			}
			return oss.str();
		};
	};
	class Point
	{
	public:
		std::complex<double> zeta;
		std::complex<double> F;
		std::complex<double> dF;
		double P;
		void get_P()
		{
			P=-0.5*(dF.real()*dF.real()+dF.imag()*dF.imag());
		}
		void joukowski_F()
		{
			dF*=(zeta*zeta/(zeta*zeta-1.));
			zeta=zeta+1./zeta;
		}
		void rotate_F(std::complex<double> zeta_0,double alpha)
		{
			std::complex<double> c;
			c.real(cos(alpha));
			c.imag(sin(alpha));
			zeta=(zeta-zeta_0)*c+zeta_0;
			dF/=c;
		}
		void rotate_Z(double alpha)
		{
			std::complex<double> c;
			c.real(cos(alpha));
			c.imag(sin(alpha));
			zeta=zeta*c;
			dF/=c;
		}
		void set_F(std::complex<double> zeta_0,double zeta_R,double gamma,std::complex<double> F_0){
			std::complex<double> c;
			c.real(0.);c.imag(2.);
			auto dzeta=zeta-zeta_0;
			F=dzeta+zeta_R*zeta_R/dzeta+c*gamma*std::log(dzeta)-F_0;
			dF=1.0-zeta_R*zeta_R/(dzeta*dzeta)+c*gamma/dzeta;
		}
	};
	myArray<Point> LO;
	myArray<Point> UP;
	std::complex<double> get_F(std::complex<double> myzeta){
		std::complex<double> c;
		c.real(0.);c.imag(2.);
		return (myzeta-zeta_0
			+zeta_R*zeta_R/(myzeta-zeta_0)
			+gamma*std::log(myzeta-zeta_0)*c);
	}
public:
	double threthold=1e-10;
	double threthold_time=1e-12;
	StreamPastArcwing(std::string path,int NR,int NTH,double alpha,double beta,double eps=0.0)
		:alpha(alpha),beta(beta),LO(NR,NTH),UP(NR,NTH),NW(NTH),eps(eps)
	{
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/path);
		gamma=(sin(alpha)+cos(alpha)*tan(beta))*(1.+eps);
		zeta_0.real(-eps);
		zeta_0.imag((1.+eps)*tan(beta));
		zeta_R=(1.+eps)/cos(beta);
		std::cout << "zeta radius " << zeta_R << " center " << zeta_0.real() << " " << zeta_0.imag() << std::endl;
		
	};
	std::string stream_tube(int NTH,int NST,double dx_sample, double xc,double yc,double rad)
	{
		//Z=(xc,yc)を中心とする半径radの円を考える. ここから出発する流管を求める.
		//角度格子はNW個, 流線格子はNR個を目標とする.
		Tube TB(NTH,NST,zeta_R,gamma);
		TB.set_C(xc,yc,rad,dx_sample);
		TB.rotate(-alpha);
		TB.i_joukowski();
		TB.rotate(+alpha,zeta_0);
		TB.shift(-zeta_0);
		//ここで流線を求める
		TB.solve_stream();
		//逆に戻していく
		TB.shift(+zeta_0);
		TB.rotate(-alpha,zeta_0);
		TB.joukowski();
		TB.rotate(+alpha);
		return TB.output();
	};
	std::string output(myArray<Point>& DATA)
	{
		int NX=DATA.size(0);
		int NY=DATA.size(1);
		std::ostringstream oss;
		oss << "# vtk DataFile Version 2.0\nえんこよくだじょ\nASCII" << std::endl;
		oss << "DATASET STRUCTURED_GRID" << std::endl;
		oss << "DIMENSIONS " << NX << " " << NY << " 1" << std::endl;
		oss << "POINTS " << NX*NY << " float" << std::endl;
		for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
			oss << DATA(i,j).zeta.real() << " "
				<< DATA(i,j).zeta.imag() << " "
				<< 0.0 << std::endl;
		oss << "POINT_DATA " << NX*NY << std::endl;
		{
			oss << "SCALARS x double" << std::endl;
			oss << "LOOKUP_TABLE default" << std::endl;
			for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
				oss << DATA(i,j).zeta.real() << std::endl;
		}
		{
			oss << "SCALARS y double" << std::endl;
			oss << "LOOKUP_TABLE default" << std::endl;
			for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
				oss << DATA(i,j).zeta.imag() << std::endl;
		}
		{
			oss << "SCALARS phi double" << std::endl;
			oss << "LOOKUP_TABLE default" << std::endl;
			for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
				oss << DATA(i,j).F.real() << std::endl;
		}
		{
			oss << "SCALARS psi double" << std::endl;
			oss << "LOOKUP_TABLE default" << std::endl;
			for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
				oss << DATA(i,j).F.imag() << std::endl;
		}
		{
			oss << "SCALARS p double" << std::endl;
			oss << "LOOKUP_TABLE default" << std::endl;
			for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
				oss << DATA(i,j).P << std::endl;
		}
		{
			oss << "VECTORS v double" << std::endl;
			for(int j=0;j<DATA.size(1);j++) for(int i=0;i<DATA.size(0);i++)
				oss
				<< DATA(i,j).dF.real() << " "
				<< -DATA(i,j).dF.imag() << " "
				<< 0.0 << std::endl;
		}
		return oss.str();

	};
	std::string wing(int n_depth=20)
	{
		//翼面の座標を計算する.
		//オリジナル円を一周する座標を出力すれば良い
		double dt=2*M_PI/(LO.size(1)-1);
		std::vector<double> TH;
		TH.resize(LO.size(1));
		for(int i=0;i<TH.size();i++)
			TH[i]=dt*i-beta;
		std::vector<std::complex<double>> W;
		W.resize(TH.size());
		std::complex<double> c;
		c.real(cos(-alpha));
		c.imag(sin(-alpha));
		for(int i=0;i<TH.size();i++)
		{
			W[i]=std::polar(zeta_R,TH[i])+zeta_0;
			W[i]=(W[i]+1./W[i])*c;
		}
		std::ostringstream oss;
		oss << "# vtk DataFile Version 2.0\nえんこよく麺だす\nASCII" << std::endl;
		oss << "DATASET STRUCTURED_GRID" << std::endl;
		oss << "DIMENSIONS " << W.size() << " " << 1 << " " << n_depth << std::endl;
		oss << "POINTS " << W.size()*n_depth << " float" << std::endl;
		for(int k=0;k<n_depth;k++)
		for(int i=0;i<W.size();i++)
			oss << W[i].real() << " "
				<< W[i].imag() << " "
				<< k << std::endl;
		return oss.str();
	};
	std::string upside()
	{
		return output(UP);
	};
	std::string loside()
	{
		return output(LO);
	};
	void make_zeta(double dr)
	{//中心zeta_0, 半径Rの円の上半分UPと下半分LOを作成
		{
			//まんずr格子
			std::vector<double> R;
			R.resize(LO.size(0));
			for(int i=0;i<LO.size(0);i++)
				R[i]=zeta_R+dr*i*i+threthold_time;
			//んだばtheta格子
			double dt=M_PI/(LO.size(1)-1);
			std::vector<double> TH;
			TH.resize(LO.size(1));
			for(int i=0;i<LO.size(1);i++)
				TH[i]=-dt*i;
			//ではzetaを作る
			for(int ir=0;ir<LO.size(0);ir++)
			for(int it=0;it<LO.size(1);it++)
				LO(ir,it).zeta=std::polar(R[ir],TH[it])+zeta_0;
		}
		{
			//まんずr格子
			std::vector<double> R;
			R.resize(UP.size(0));
			for(int i=0;i<UP.size(0);i++)
				R[i]=zeta_R+dr*i*i+threthold_time;
			//んだばtheta格子
			double dt=M_PI/(UP.size(1)-1);
			std::vector<double> TH;
			TH.resize(UP.size(1));
			for(int i=0;i<UP.size(1);i++)
				TH[i]=+dt*i;
			//ではzetaを作る
			for(int ir=0;ir<UP.size(0);ir++)
			for(int it=0;it<UP.size(1);it++)
				UP(ir,it).zeta=std::polar(R[ir],TH[it])+zeta_0;
		}
	};
	void make_F()
	{
		//淀み点における関数値を求める
		std::complex<double> stag_zeta=zeta_0;
		stag_zeta.real(stag_zeta.real()-sqrt(zeta_R*zeta_R-gamma*gamma));
		stag_zeta.imag(stag_zeta.imag()-gamma);
		auto F0=get_F(stag_zeta);
		//円筒をすぎる流れ場を設定
		for(auto& P:LO) P.set_F(zeta_0,zeta_R,gamma,F0);
		for(auto& P:UP) P.set_F(zeta_0,zeta_R,gamma,F0);
	};
	void rotate_F()
	{
		for(auto& P:LO) P.rotate_F(zeta_0,alpha);
		for(auto& P:UP) P.rotate_F(zeta_0,alpha);
	};
	void rotate_Z(double delta)
	{
		for(auto& P:LO) P.rotate_Z(delta);
		for(auto& P:UP) P.rotate_Z(delta);
	};
	void get_P()
	{
		for(auto& P:LO) P.get_P();
		for(auto& P:UP) P.get_P();
	}
	void joukowski_F()
	{
		for(auto& P:LO) P.joukowski_F();
		for(auto& P:UP) P.joukowski_F();
	};
};
class StreamPastFairing
{//フェアリングを過ぎる3D流れの流線
	double Xmin,Xmax,dX;
	int NX,NTH;
	std::vector<double> x0;
	std::vector<double> y0;
	std::vector<double> z0;
	std::vector<double> c0;
	void clear()
	{
		x0.clear();
		y0.clear();
		z0.clear();
		c0.clear();
	}
	std::string calc(int NX,int NTH,bool sign=true)
	{
		//流管を計算
		myArray<double> val(5,NX+1,NTH+1);
		for(int ith=0;ith<=NTH;ith++) for(int ix=0;ix<=NX;ix++)
		{
			val(0,ix,ith)=Xmin+ix*dX;
			double RR=Solve(val(0,ix,ith),c0[ith]);
			double TH=atan2(z0[ith],y0[ith]);
			val(1,ix,ith)=RR*cos(TH);
			val(2,ix,ith)=RR*sin(TH);
			val(3,ix,ith)=ith*dth;
		}
		//経過時間を計算
		IntDE myINT;
		double C;
		myINT.Parameter=&C;
		for(int ith=0;ith<=NTH;ith++) for(int ix=0;ix<=NX;ix++)
		{
			double R0=sqrt(val(1,0,ith)*val(1,0,ith)+val(2,0,ith)*val(2,0,ith));
			double RR=sqrt(val(1,ix,ith)*val(1,ix,ith)+val(2,ix,ith)*val(2,ix,ith));
			double err_v;
			C=c0[ith];
			myINT.Initialize(threthold_time);
			auto F=[](double y, void *p){
				auto& C=*(double *)p;
				double DX=1.-2.*(C-y*y);
				double DD=(1.-DX*DX);
				return 4*y*y/(DD*sqrt(DD));
			};
			val(4,ix,ith)=myINT.Integrate(F,R0,RR,err_v);
		}
		//出力
		std::ostringstream oss;
		oss << "# vtk DataFile Version 2.0\nりゅうせんだっちゅーの\nASCII" << std::endl;
		oss << "DATASET STRUCTURED_GRID" << std::endl;
		oss << "DIMENSIONS " << NX+1 << " " << NTH+1 << " 1" << std::endl;
		oss << "POINTS " << (NX+1)*(NTH+1) << " float" << std::endl;
		for(int ith=0;ith<=NTH;ith++ ) for(int ix=0;ix<=NX;ix++)
			oss << val(0,ix,ith) << " " << val(1,ix,ith) << " " << val(2,ix,ith) << std::endl;
		oss << "POINT_DATA " << (NX+1)*(NTH+1) << std::endl;
		{
			oss << "SCALARS theta double" << std::endl;
			oss << "LOOKUP_TABLE default" << std::endl;
			for(int ith=0;ith<=NTH;ith++ ) for(int ix=0;ix<=NX;ix++) oss << val(3,ix,ith) << std::endl;
		}
		{
			oss << "SCALARS X double" << std::endl;
			oss << "LOOKUP_TABLE default" << std::endl;
			for(int ith=0;ith<=NTH;ith++ ) for(int ix=0;ix<=NX;ix++) oss << val(0,ix,ith) << std::endl;
		}
		{
			oss << "SCALARS time double" << std::endl;
			oss << "LOOKUP_TABLE default" << std::endl;
			for(int ith=0;ith<=NTH;ith++ ) for(int ix=0;ix<=NX;ix++) oss << val(4,ix,ith) << std::endl;
		}
		return oss.str();

	}
	double Solve(double X,double C)
	{
		double a=sqrt(C-1),b=sqrt(C),Y;
		unsigned int count=0;
		double err=1.0;
		while ((std::abs(err)>threthold)&&count++<1000)
		{
			Y=(a+b)/2;
			err=2*(Y*Y-C)-X/sqrt(X*X+Y*Y)+1;
			if (err<0) a=Y; else b=Y;
		}
		return Y;
	};
public:
	double threthold=1e-10;
	double threthold_time=1e-15;
	double dth=1;
	StreamPastFairing(std::string path,double Xmin,double Xmax,double dX)
	:Xmin(Xmin),Xmax(Xmax),dX(dX)
	{
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/path);
		NX=(Xmax-Xmin)/dX;
		NTH=360/dth;
	};
	std::string full(double XC,double YC,double ZC,double RAD)
	{
		//初期リング
		clear();
		for(int ith=0;ith<=NTH;ith++)
		{
			double theta=ith*dth/180*M_PI;
			x0.push_back(XC);
			y0.push_back(YC+RAD*cos(theta));
			z0.push_back(ZC+RAD*sin(theta));
			double _r2=y0[ith]*y0[ith]+z0[ith]*z0[ith];
			c0.push_back(_r2+0.5-0.5*x0[ith]/sqrt(x0[ith]*x0[ith]+_r2));
		}
		return calc(NX,NTH);
	}
	std::string draw_Fairing()
	{
		int NFX=(Xmax-0.5)/dX;
		std::ostringstream oss;
		oss << "# vtk DataFile Version 2.0\nフェアリングだぢょ\nASCII" << std::endl;
		oss << "DATASET STRUCTURED_GRID" << std::endl;
		oss << "DIMENSIONS " << NFX+1 << " " << NTH+1 << " 1" << std::endl;
		oss << "POINTS " << (NFX+1)*(NTH+1) << " float" << std::endl;
		for(int ith=0;ith<=NTH;ith++) for(int ix=0;ix<=NFX;ix++)
		{
			double th=ith*dth/360*2*M_PI;
			double xx=-0.5+ix*ix*dX/NFX;
			double x2=xx*xx;
			double rr=xx>0?sqrt((1-x2+sqrt(x2*x2+2*x2))/2):sqrt((1-x2-sqrt(x2*x2+2*x2))/2);
			double yy=rr*cos(th);
			double zz=rr*sin(th);
			oss << xx << " " << yy << " " << zz << std::endl;
		}
		return oss.str();
	};
};
class StreamPastCylinder
{//円柱をすぎる2D流れの流線
	double Xmin,Xmax,dX;
	int NX,NTH;
	std::vector<double> x0;
	std::vector<double> y0;
	std::vector<double> z0;
	std::vector<double> c0;
	double Solve(double X,double C)
	{
		double a=C,b=(C+std::sqrt(C*C+4))/2,Y;
		unsigned int count=0;
		double err=1.0;
		while ((std::abs(err)>threthold)&&count++<1000)
		{
			Y=(a+b)/2;
			err=Y*(1/(Y-C)-Y)-X*X;
			if (err<0) b=Y; else a=Y;
		}
		return Y;
	};
	std::string calc(int NX,int NTH,bool sign=true)
	{
		//流管を計算
		myArray<double> val(5,NX+1,NTH+1);
		for(int ith=0;ith<=NTH;ith++) for(int ix=0;ix<=NX;ix++)
		{
			val(0,ix,ith)=Xmin+ix*dX;
			val(1,ix,ith)=Solve(val(0,ix,ith),c0[ith]);
			val(2,ix,ith)=z0[ith];
			val(3,ix,ith)=ith*dth;
		}
		//経過時間を計算
		IntDE myINT;
		double C;
		myINT.Parameter=&C;
		for(int ith=0;ith<=NTH;ith++) for(int ix=0;ix<=NX;ix++)
		{
			double err_v;
			C=c0[ith];
			myINT.Initialize(threthold_time);
			auto F=[](double y, void *p){
				auto& C=*(double *)p;
				   double yC=y-C;
				   return 0.5*std::sqrt(y*yC/(1-y*yC))/(yC*yC);
			};
			if (val(0,ix,ith)<0)
				val(4,ix,ith)=myINT.Integrate(F,y0[ith],val(1,ix,ith),err_v);
			else
			{
				double ymax=Solve(0.,c0[ith]);
				val(4,ix,ith)=myINT.Integrate(F,y0[ith],ymax,err_v)
							 +myINT.Integrate(F,val(1,ix,ith),ymax,err_v);

			}
		}
		//出力
		std::ostringstream oss;
		oss << "# vtk DataFile Version 2.0\n流線だぞ〜\nASCII" << std::endl;
		oss << "DATASET STRUCTURED_GRID" << std::endl;
		oss << "DIMENSIONS " << NX+1 << " " << NTH+1 << " 1" << std::endl;
		oss << "POINTS " << (NX+1)*(NTH+1) << " float" << std::endl;
		if (sign)
			for(int ith=0;ith<=NTH;ith++ ) for(int ix=0;ix<=NX;ix++)
				oss << val(0,ix,ith) << " " << val(1,ix,ith) << " " << val(2,ix,ith) << std::endl;
		else
			for(int ith=0;ith<=NTH;ith++ ) for(int ix=0;ix<=NX;ix++)
				oss << val(0,ix,ith) << " " << -val(1,ix,ith) << " " << val(2,ix,ith) << std::endl;
		oss << "POINT_DATA " << (NX+1)*(NTH+1) << std::endl;
		oss << "SCALARS theta double" << std::endl;
		oss << "LOOKUP_TABLE default" << std::endl;
		for(int ith=0;ith<=NTH;ith++ ) for(int ix=0;ix<=NX;ix++)
			oss << val(3,ix,ith) << std::endl;
		oss << "SCALARS time double" << std::endl;
		oss << "LOOKUP_TABLE default" << std::endl;
		for(int ith=0;ith<=NTH;ith++ ) for(int ix=0;ix<=NX;ix++)
			oss << val(4,ix,ith) << std::endl;
		return oss.str();
	}
	void clear()
	{
		x0.clear();
		y0.clear();
		z0.clear();
		c0.clear();
	}
public:
	double threthold=1e-10;
	double threthold_time=1e-15;
	double dth=1;
	std::string upside(double XC,double ZC,double RAD)
	{
		double YC=0;
		//初期リング
		clear();
		for(int ith=0;ith<=NTH;ith++)
		{
			double theta=ith*dth/360*M_PI;
			if (ith==0) theta=threthold;
			if (ith==NTH) theta=M_PI-threthold;
			x0.push_back(XC);
			y0.push_back(YC+RAD*cos(theta-M_PI/2));
			z0.push_back(ZC+RAD*sin(theta-M_PI/2));
			double _r2=x0[ith]*x0[ith]+y0[ith]*y0[ith];
			c0.push_back(y0[ith]*(_r2-1)/_r2);
		}
		return calc(NX,NTH);
	}
	std::string loside(double XC,double ZC,double RAD)
	{
		double YC=0;
		//初期リング
		clear();
		for(int ith=0;ith<=NTH;ith++)
		{
			double theta=ith*dth/360*M_PI;
			if (ith==0) theta=threthold;
			if (ith==NTH) theta=M_PI-threthold;
			x0.push_back(XC);
			y0.push_back(YC+RAD*cos(theta-M_PI/2));
			z0.push_back(ZC+RAD*sin(theta-M_PI/2));
			double _r2=x0[ith]*x0[ith]+y0[ith]*y0[ith];
			c0.push_back(y0[ith]*(_r2-1)/_r2);
		}
		return calc(NX,NTH,false);
	}
	std::string full(double XC,double YC,double ZC,double RAD)
	{
		//初期リング
		clear();
		for(int ith=0;ith<=NTH;ith++)
		{
			double theta=ith*dth/180*M_PI;
			x0.push_back(XC);
			y0.push_back(YC+RAD*cos(theta));
			z0.push_back(ZC+RAD*sin(theta));
			double _r2=x0[ith]*x0[ith]+y0[ith]*y0[ith];
			c0.push_back(y0[ith]*(_r2-1)/_r2);
		}
		return calc(NX,NTH);
	}
	StreamPastCylinder(std::string path,double Xmin,double Xmax,double dX)
		:Xmin(Xmin),Xmax(Xmax),dX(dX)
	{
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/path);
		NX=(Xmax-Xmin)/dX;
		NTH=360/dth;
	};
};

#endif /* make_vtk_hpp */
