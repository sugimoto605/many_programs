//
//  heat.hpp
//  SPARCE
//
//  Created by Hiroshi Sugimoto on 2021/06/03.
//

#ifndef heat_hpp
#define heat_hpp

#include <sstream>
#include <boost/filesystem.hpp>
#include "libSPARSE.hpp"
#include "myArray.hpp"

class Heat
{
	double xmax=1.;
	double u_x0=0.,u_x1=0.,u_y0=1.,u_y1=0.;	//境界条件
	double dx,dt,cfl;
	myArray<double> mySOL;
	SPARSE::Coef myMAT;
public:
	Heat(int N,double dt):mySOL(N-1,N-1)
	{
		dx=xmax/N;
		cfl=dt/dx/dx;
		for(int i=0; i<mySOL.size(0);i++)
		for(int j=0; j<mySOL.size(1);j++)
		{
			double x=(i+1)*dx,y=(j+1)*dx;
			mySOL(i,j)=(1+y-2*y*y)*(1-4*(x-0.5)*(x-0.5));
		}
		//行列サイズは mySOL.size()xmySOL.size()になる
		for(int k=0;k<mySOL.size();k++) myMAT.New(k,1.0+4*cfl);//対角成分は 1+4*cfl
		for(int k=0;k<mySOL.size();k++) myMAT[k][myMAT.Dimension()]=0.;
		for(int k=0;k<mySOL.size();k++)//他の要素を入れる
		{
			int i=mySOL.index(0,k);			//k番目の未知数のi
			int j=mySOL.index(1,k);			//k番目の未知数のj
			if (i==0)				myMAT[k][myMAT.Dimension()]+=cfl*u_x0;
			else					myMAT[k][mySOL.serial(i-1,j)]=-cfl;
			if (i==mySOL.size(0)-1)	myMAT[k][myMAT.Dimension()]+=cfl*u_x1;
			else					myMAT[k][mySOL.serial(i+1,j)]=-cfl;
			if (j==0)				myMAT[k][myMAT.Dimension()]+=cfl*u_y0;
			else					myMAT[k][mySOL.serial(i,j-1)]=-cfl;
			if (j==mySOL.size(1)-1)	myMAT[k][myMAT.Dimension()]+=cfl*u_y1;
			else					myMAT[k][mySOL.serial(i,j+1)]=-cfl;
		}
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/"tes_test");
	}
	double Step(int nt)
	{
		auto thisMAT=myMAT;
		for(int k=0;k<mySOL.size();k++)
			thisMAT[k][thisMAT.Dimension()]+=mySOL[k];
		thisMAT.MakeIndex();
		thisMAT.EraseUp();
		thisMAT.EraseDown();
		thisMAT.ClearIndex();
		double err=0.0;
		for(int k=0;k<mySOL.size();k++)
		{
			err=std::max(std::abs(thisMAT[k][thisMAT.Dimension()]-mySOL[k]),err);
			mySOL[k]=thisMAT[k][thisMAT.Dimension()];
		}
		return err;
	}
	void vtk(int nt)
	{
		std::ostringstream oss;
		oss << "vtk_" <<  std::setfill('0') << std::right << std::setw(6) << nt << ".vtk";
		std::ofstream vout(oss.str());
		vout << "# vtk DataFile Version 2.0" << std::endl << "ひゃっほう" << std::endl << "ASCII" << std::endl;
		//Geometry指定
		vout << "DATASET STRUCTURED_POINTS" << std::endl;
		vout << "DIMENSIONS " << mySOL.size(0)+2 << " " << mySOL.size(1)+2 << " " << 1 << std::endl;
		vout << "ORIGIN 0 0 0" << std::endl;
		vout << "SPACING " << 1.0/(mySOL.size(0)+1) << " " << 1.0/(mySOL.size(1)+1) << " " << 1.0 << std::endl;
		//DataSet指定
		vout << "POINT_DATA " << (mySOL.size(0)+2)*(mySOL.size(1)+2) << std::endl;
		vout << "SCALARS U double" << std::endl;
		vout << "LOOKUP_TABLE default" << std::endl;
		for(int j=0;j<mySOL.size(1)+2;j++)
			for(int i=0;i<mySOL.size(0)+2;i++)
			{
				if (i==0)
					vout << u_x0 <<std::endl;
				else if (i==mySOL.size(0)+1)
					vout << u_x1 <<std::endl;
				else
				{
					if (j==0)
						vout << u_y0 <<std::endl;
					else if (j==mySOL.size(1)+1)
						vout << u_y1 <<std::endl;
					else
						vout << mySOL(i-1,j-1) <<std::endl;
				}
			}
	}
};

#endif /* heat_hpp */
