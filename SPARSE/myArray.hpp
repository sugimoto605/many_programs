//
//  myArray.hpp
//  SPARCE
//
//  Created by Hiroshi Sugimoto on 2021/06/03.
//

#ifndef myArray_hpp
#define myArray_hpp

#include <iostream>
#include <vector>

template <class T> class myArray
{
	std::vector<int> _sizes;
	std::vector<int> _upto;
	std::vector<T> _data;
public:
	T* data() {return _data.data();}
	const int size(){return _upto.back();}
	const int size(const int index){return _sizes.at(index);}
	T& operator[](const int k){return _data[k];}
	typename std::vector<T>::iterator begin(){return _data.begin();}
	typename std::vector<T>::iterator end(){return _data.end();}
	const int index(const int dim,const int k)
	{
		int _index=k;
		for(int i=0;i<dim;i++) _index/=_sizes[i];
		if (dim==_sizes.size()-1) return _index;
		return _index%_sizes[dim];
	}
	myArray(const int n0,const int n1)
	{
		_sizes.resize(2);	//2次元配列である
		_upto.resize(2);
		_sizes[0]=n0;	_sizes[1]=n1;
		_upto[0]=n0;	_upto[1]=_upto[0]*n1;
		_data.resize(size());
	}
	const int serial(const int i0,const int i1){return i0+_upto[0]*i1;}
	T& operator()(const int i0,const int i1){return _data[serial(i0,i1)];}
	myArray(const int n0,const int n1,const int n2)
	{
		_sizes.resize(3);	//3次元配列である
		_upto.resize(3);
		_sizes[0]=n0;	_sizes[1]=n1;			_sizes[2]=n2;
		_upto[0]=n0;	_upto[1]=_upto[0]*n1;	_upto[2]=_upto[1]*n2;
		_data.resize(size());
	}
	const int serial(const int i0,const int i1,const int i2){return i0+_upto[0]*i1+_upto[1]*i2;}
	T& operator()(const  int i0,const int i1,const int i2){return _data[serial(i0,i1,i2)];}
};

#endif /* myArray_hpp */
