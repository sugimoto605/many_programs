//
//  maine.cpp
//  SPARCE
//
//  Created by Hiroshi Sugimoto on 2021/06/02.
//

#include <iostream>
#include "heat.hpp"

int main(int argc, const char * argv[]) {
	int N=50,nt=0;
	double dt=0.1;
	Heat Solver(N,dt);
	Solver.vtk(nt);
	double errmax=1e-5,err;
	while (errmax<(err=Solver.Step(nt++)))
	{
		Solver.vtk(nt);
		std::cout << "nt= " << nt << " err= " << err << std::endl;
	}
	return 0;
}
