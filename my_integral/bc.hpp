//
//  bc.hpp
//  my_integral
//
//  Created by Hiroshi Sugimoto on 2021/05/16.
//

#ifndef bc_hpp
#define bc_hpp

#include <iostream>
#include <cmath>
#include <boost/filesystem.hpp>

class BC
{
public:
	class myMat
	{
	public:
		double A;
		double B;
		double C;
	};
	std::vector<myMat> myMAT;
	std::vector<double> myVEC;
	std::vector<double> mySOL;
	double xmax=1.0;
	BC(int N)
	{
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/"tes_test");
		std::ofstream ofs("my3.data");
		myMAT.resize(N);
		myVEC.resize(N);
		mySOL.resize(N);
		double C=200.;
		double dx=xmax/N;
		double y_min=1.0;
		double y_max=0.0;
		for(int i=1;i<N;i++)
		{
			if (i>1) myMAT[i].A=-1.;
			myMAT[i].B=2.-C*dx*dx;
			if (i<N-1) myMAT[i].C=-1.;
			if (i==1)
				myVEC[i]=y_min;
			else if (i==(N-1))
				myVEC[i]=y_max;
			else
				myVEC[i]=0.;
		}
		for(int i=2;i<N;i++)
		{
			double rate=myMAT[i].A/myMAT[i-1].B;
			myMAT[i].A+=-rate*myMAT[i-1].B;// 0になるので無駄計算
			myMAT[i].B+=-rate*myMAT[i-1].C;
			myVEC[i]+=-rate*myVEC[i-1];
		}
		mySOL[N-1]=myVEC[N-1]/myMAT[N-1].B;
		for(int i=N-2;i>0;i--)
		  mySOL[i]=(myVEC[i]-mySOL[i+1]*myMAT[i].C)/myMAT[i].B;
		mySOL[0]=y_min;
		mySOL[N]=y_max;
		for(int i=0;i<=N;i++) ofs << dx*i << " " << mySOL[i] << std::endl;
	};
	~BC()
	{
		std::cout << "終わったぞこのタコやろう" << std::endl;
	}
};

#endif /* bc_hpp */
