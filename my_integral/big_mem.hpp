//
//  big_mem.hpp
//  my_integral
//
//  Created by Hiroshi Sugimoto on 2021/05/17.
//

#ifndef big_mem_hpp
#define big_mem_hpp

#include <iostream>
#include <cmath>
#include <boost/filesystem.hpp>
#include <Eigen/Dense>

class BIG_MEM
{
private:
	Eigen::MatrixXd myMAT;
	Eigen::VectorXd myVEC;
	int N;
	double xmax=1.0;
	double dx;
	double y_min,y_max;
	double C=2.;
	std::ofstream ofs;
public:
	BIG_MEM(int _N)
	{
		N=_N;
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/"tes_test");
		dx=xmax/N;
		y_min=1.0;
		y_max=0.0;
		myMAT.resize(N-1,N-1);
		myMAT.setZero();
		myVEC.resize(N-1);
		myVEC.setZero();
		for(int i=0;i<N-1;i++)
		{
			if (i>0) myMAT(i,i-1)=-1.;
			myMAT(i,i)=2.-C*dx*dx;
			if (i<N-2) myMAT(i,i+1)=-1.;
			if (i==0) myVEC(i)=y_min;
			else if (i==(N-2)) myVEC[i]=y_max;
		}
	}
	Eigen::MatrixXd MAT;
	Eigen::VectorXd VEC;
	Eigen::VectorXd SOL;
	void Ready()
	{
		MAT.resize(N-1,N-1);
		MAT.setZero();
		VEC.resize(N-1);
		VEC.setZero();
		SOL.resize(N-1);
		SOL.setZero();
		double dia=2.-C*dx*dx;
		for(int i=0;i<N-1;i++)
		{
			if (i>0) MAT(i,i-1)=-1./dia;
			if (i<N-2) MAT(i,i+1)=-1./dia;
			if (i==0) VEC(i)=y_min/dia;
			else if (i==(N-2)) VEC[i]=y_max/dia;
		}
		ofs.open("my.data");
	}
	double Fire()
	{
		auto NEW=VEC-MAT*SOL;
		double err=(NEW-SOL).squaredNorm();
		SOL=NEW;
		std::cout << "ERR= " << err << std::endl;
		return err;
	}
	void Write()
	{
		ofs << dx*0 << " " << y_min << std::endl;
		for(int i=0;i<N-1;i++)
			ofs << dx*(i+1) << " " << SOL[i] << std::endl;
		ofs << dx*N << " " << y_max << std::endl << std::endl;
	}
	void INV()
	{
		auto SOL=myMAT.inverse()*myVEC;
		ofs << dx*0 << " " << y_min << std::endl;
		for(int i=0;i<N-1;i++)
			ofs << dx*(i+1) << " " << SOL[i] << std::endl;
		ofs << dx*N << " " << y_max << std::endl;
	}

};

#endif /* big_mem_hpp */
