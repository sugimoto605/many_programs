//
//  integral.hpp
//  my_integral
//
//  Created by Hiroshi Sugimoto on 2021/05/18.
//

#ifndef integral_hpp
#define integral_hpp

#include <iostream>
#include <cmath>
#include <boost/filesystem.hpp>

class DET
{
public:
	class Parameter
	{
	public:
		int count=0;
	};
	Parameter parm;
	DET()
	{
		IntDE myINT;
		myINT.Parameter=&parm;
		double xmin=0,xmax=0.5,err,err_request=1e-12;
		myINT.Initialize(err_request);
		double v=myINT.Integrate([](double x, void *p){
			auto& P=*(Parameter *)p;
			P.count++;
			return 2./sqrt(x*(1.-x))/M_PI;
		},xmin,xmax,err)-1.0;
		std::cout << "ERROR=" << v << std::endl;
		std::cout << "Used " << parm.count << " err= " << err << std::endl;
	}
};

class P1
{
public:
	double integrand(double th,double U)
	{
		auto UC=U*std::cos(th);
		auto US=U*sin(th);
		auto ERF=std::erf(UC);
		auto EXP=std::exp(-US*US);
		auto COEF=M_2_SQRTPI/4.;
		return COEF*EXP*ERF*UC;
	}
	double p2f(double th)
	{
		return 2./sqrt(th*(1.-th))/M_PI;
	}
	P1(double U,int N,double eps)
	{
		double th_min=eps;
		double th_max=0.5;
		double dth=(th_max-th_min)/N;
		double sum=0;
		for(int i=0;i<N;i++)
		{
			double y0=p2f(th_min+dth*i);
			double y1=p2f(th_min+dth*(i+1));
			sum+=dth/2*(y0+y1);
		}
		sum+=-1.;
		std::cout << "U= " << U << " N= " << N << " ERROR= " << sum << std::endl;
	}
};
#endif /* integral_hpp */
