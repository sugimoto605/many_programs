//
//  my_first_solver.hpp
//  my_integral
//
//  Created by Hiroshi Sugimoto on 2021/05/16.
//

#ifndef my_first_solver_hpp
#define my_first_solver_hpp

#include <iostream>
#include <cmath>
#include <boost/filesystem.hpp>
class ODE
{
public:
	ODE(){
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/"tes_test");
	};
	void solve2(){
		std::ofstream ofs("my3.data");
		int N=40;				//100等分してみる
		double xmax=2*M_PI;		//x=1まで解く
		double dx=xmax/N;		//ってこた区間幅はこうなる
		double y=1.,z=0.;		//初期値は y=1, z=0
		ofs << 0.0 << " " << y << std::endl;
		for(int i=0;i<N;i++)
		{
			y=y+z*dx;
			z=z-y*dx;
			ofs << dx*(i+1) << " " << y << std::endl;
		}
	}
	void solve(){
		std::ofstream ofs("my.data");
		std::cout << "ほんじゃま, 解いてみっか？" << std::endl;
		int N=100;				//100等分してみる
		double xmax=1.0;		//x=1まで解く
		double dx=xmax/N;		//ってこた区間幅はこうなる
		double y=1.;			//初期値は y=1
		ofs << 0.0 << " " << y << std::endl;
		for(int i=0;i<N;i++)
		{
			y=y-y*dx;
			ofs << dx*(i+1) << " " << y << std::endl;
		}
		ofs.close();
		std::cout << "とけたぞコラ" << std::endl;
	}
};
#endif /* my_first_solver_hpp */
