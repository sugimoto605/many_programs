//
//  main.cpp
//  PDE
//
//  Created by Hiroshi Sugimoto on 2021/05/17.
//

#include <iostream>
//#include "convect.hpp"
#include "wave.hpp"

int main(int argc, const char * argv[]) {
	double dt=0.0001;
	double tmax=3,tsave=1;
	double xmax=10;
	int nx=100;
	int ntmax=tmax/dt,ntsave=tsave/dt;
	Solver mySolver(xmax,nx,dt,"kaku","pde_10.data");
	mySolver.Write(0);
	for(int nt=0;nt<ntmax;nt++)
	{
		mySolver.Step(nt);
		if ((nt+1)%ntsave==0) mySolver.Write(nt+1);
	}
	return 0;
}
