//
//  convect.hpp
//  PDE
//
//  Created by Hiroshi Sugimoto on 2021/05/17.
//

#ifndef convect_hpp
#define convect_hpp

#include <iostream>
#include <cmath>
#include <boost/filesystem.hpp>

class Solver
{
	std::vector<double> U_VAL;
	std::vector<double> U_OLD;
	double CFL;
	double xmax=10;
	double c=1.0;
	double UW=1.0;
	double U0=0.0;
	double dx;
	double dt;
	double time=0;
	std::ofstream ofs;
	std::ofstream exact;
public:
	Solver(double xmax,int N,const double dt,std::string filename):xmax(xmax),dt(dt)
	{
		boost::filesystem::path folder=getenv("HOME");
		boost::filesystem::current_path(folder/"tes_test");
		dx=xmax/N;
		U_VAL.resize(N+1);
		U_OLD.resize(U_VAL.size());
		for(auto& U:U_VAL) U=U0;
		U_VAL[0]=UW;
		CFL=c*dt/dx;
		ofs.open(filename);
		exact.open("exact.dat");
	}
	void Step(int nt)
	{
		for(int i=0;i<U_VAL.size();i++) U_OLD[i]=U_VAL[i];
		for(int i=1;i<U_VAL.size();i++)
			U_VAL[i]=(1.-CFL)*U_OLD[i]+CFL*U_OLD[i-1];
		time+=dt;
	}
	void Write(int nt)
	{
		for(int i=0;i<U_VAL.size();i++)
			ofs << dx*i << " " << U_VAL[i] << std::endl;
		ofs<<std::endl;
		exact << 0 << " " << UW << std::endl;
		exact << c*time << " " << UW << std::endl;
		exact << c*time << " " << U0 << std::endl;
		exact << xmax << " " << U0 << std::endl << std::endl;
	}
};

#endif /* convect_hpp */
