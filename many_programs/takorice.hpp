//
//  takorice.hpp
//  many_programs
//
//  Created by Hiroshi Sugimoto on 2021/05/15.
//

#ifndef takorice_hpp
#define takorice_hpp

#include <iostream>
class tako_rice
{
public:
	std::string my_name;
	tako_rice(){
		std::cout << "ぼなぺち" << std::endl;
	}
	~tako_rice(){
		std::cout << "よろしうおあがり[" << my_name << "]" << std::endl;
	}
};
#endif /* takorice_hpp */
