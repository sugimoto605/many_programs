//
//  main.cpp
//  many_programs
//
//  Created by Hiroshi Sugimoto on 2021/05/15.
//
//	生まれて初めてC++を使う人用のサンプル．
//	解説は http://fd.kuaero.kyoto-u.ac.jp/ja/node/850

#include <iostream>
#include "takorice.hpp"

int main(int argc, const char * argv[]) {
	tako_rice yummy_TACO;
	yummy_TACO.my_name="おにぎり";
	tako_rice good_TACO;
	good_TACO.my_name="ドーナッツ";
}
